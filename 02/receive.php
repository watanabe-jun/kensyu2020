<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>第二回課題、フォーム部品練習</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>

    <body>
        <div class="container">

            <h1>第二回課題、フォーム部品練習</h1>

            <?php
                if(empty($_GET['name']) and empty($_GET['email'])){
                    echo '<div class="panel panel-danger">';
                    echo '<div class="panel-heading">エラー</div>';
                    echo '<div class="panel-body">';
                    echo "お名前とメールアドレスは必須入力項目です。";
                    echo '</div>';
                } else if(array_key_exists('name', $_GET) and array_key_exists('email', $_GET)) {
                    if(isset($_GET['name']) and isset($_GET['email'])) {
                        echo '<div class="panel panel-info">';
                        echo '<div class="panel-heading">ようこそ</div>';
                        echo '<div class="panel-body">';
                        echo "ようこそ " . $_GET['name'] . " さん (" . $_GET['email'] . ")<br/>";
                    }
                    if(array_key_exists('gender', $_GET)) {
                        if(isset($_GET['name'])) {
                            echo "あなたは " . $_GET['gender'] . "性で、<br/>";
                        }
                    }
                    echo "趣味は ";
                    if(empty($_GET['hobby'])) {
                        echo "特になし";
                    } else {
                        foreach($_GET['hobby'] as $value) {
                            if($value === reset($_GET['hobby'])) {
                                // 最初
                                echo $value;
                            } else {
                                echo ", " . $value;
                            }
                        }
                    }
                    echo"ですね。<br/>";
                    echo '</div>';
                }
            ?>
            </div>

            <div class="panel panel-info">
                <div class="panel-heading">var_dump</div>

                <div class="panel-body">
                    <pre>
                        <?php
                            var_dump($_GET);
                        ?>
                    </pre>
                </div>
            </div>
        </div>



    </body>
</html>
