<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>社内開発研修 03</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">    </head>
    </head>

    <body>
        <div class="container">

            <?php if(isset($_POST['cols-01']) and $_POST['cols-01'] === ""): ?>
                <div class="alert alert-danger my-3 alert-dismissible fade show" role="alert">
                    <p><strong>テーブル生成 01</strong></p>
                    <hr>
                    <p>生成する行数を入力してください。</p>

                    <button type="button" class="close" data-dismiss="alert" aria-label="閉じる">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>

            <?php if(isset($_POST['cols-02']) and $_POST['cols-02'] === ""): ?>
                <div class="alert alert-danger my-3 alert-dismissible fade show" role="alert">
                    <p><strong>テーブル生成 02</strong></p>
                    <hr>
                    <p>生成する行数を入力してください。</p>

                    <button type="button" class="close" data-dismiss="alert" aria-label="閉じる">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>

            <?php if(isset($_POST['rows-02']) and $_POST['rows-02'] === ""): ?>
                <div class="alert alert-danger my-3 alert-dismissible fade show" role="alert">
                    <p><strong>テーブル生成 02</strong></p>
                    <hr>
                    <p>生成する列数を入力してください。</p>

                    <button type="button" class="close" data-dismiss="alert" aria-label="閉じる">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>

            <!-- テーブル生成 [1] -->

            <div class="card my-3">
                <h3 class="card-header bg-primary text-white">テーブル生成 01</h3>

                <div class="card-body">
                    <form action="#" method="post" id="table-form-01">
                        <div class="form-group">
                            <label for="cols-01">生成する行数</label>
                            <input class="form-control" name="cols-01" type="number" placeholder="生成する行数を入力してください。">
                            <!-- n行のテーブルを生成する -->
                        </div>

                        <div class="float-left">
                            <button type="submit" class="btn btn-primary">送信</button>
                            <button type="reset" class="btn page-link text-dark d-inline-block">リセット</button>
                        </div>
                    </form>
                </div>
            </div>

            <?php if(isset($_POST['cols-01']) and $_POST['cols-01'] != ""): ?>

            <div class="card my-3">
                <h3 class="card-header bg-secondary text-white">テーブル生成 01 - 結果</h3>
                <div class="card-body">
                    <table class="table table-bordered table-sm" style="table-layout:fixed;">
                        <?php if(!empty($_POST['cols-01'])): ?>
                            <?php for($i = 1; $i <= $_POST['cols-01']; $i++): ?>
                                <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $i ?></td>
                                </tr>
                            <?php endfor; ?>
                        <?php endif; ?>
                    </table>
                </div>
            </div>

            <?php endif; ?>

            <!-- テーブル生成 [2] -->

            <div class="card my-3">
                <h3 class="card-header bg-primary text-white">テーブル生成 02</h3>

                <div class="card-body">
                    <form action="#" method="post" id="table-form-01">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-text">行</span>
                                <input class="form-control" name="cols-02" type="number" aria-label="行">
                            </div>

                            <div class="input-group">
                                <span class="input-group-text">列</span>
                                <input class="form-control" name="rows-02" type="number" aria-label="列">
                            </div>
                        </div>

                        <div class="float-left">
                            <button type="submit" class="btn btn-primary">送信</button>
                            <button type="reset" class="btn page-link text-dark d-inline-block">リセット</button>
                        </div>
                    </form>
                </div>
            </div>

            <?php if((isset($_POST['cols-02']) and $_POST['cols-02'] != "") and (isset($_POST['rows-02']) and $_POST['rows-02'] != "")): ?>

            <div class="card my-3">
                <h3 class="card-header bg-secondary text-white">テーブル生成 02 - 結果</h3>
                <div class="card-body">
                    <table class="table table-bordered table-sm" style="table-layout:fixed;">
                        <?php if(!empty($_POST['cols-02'])): ?>
                            <?php for($i = 1; $i <= $_POST['cols-02']; $i++): ?>
                                <tr>
                                    <?php if(!empty($_POST['rows-02'])): ?>
                                        <?php for($j = 1; $j <= $_POST['rows-02']; $j++): ?>
                                            <?php if($i % 2 === 0): ?>
                                                <td class="bg-secondary text-white"><?php echo $i . " - " . $j ?></td>
                                            <?php else: ?>
                                                <td class="bg-primary text-white"><?php echo $i . " - " . $j ?></td>
                                            <?php endif; ?>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                                </tr>
                            <?php endfor; ?>
                        <?php endif; ?>
                    </table>
                </div>
            </div>

            <?php endif; ?>

            <!--
            <div class="card my-3">
                <h3 class="card-header bg-info text-white">var_dump : output</h3>

                <div class="card-body">
                    <pre class="border border-info rounded">
                        <code>
                            <?php
                                echo var_dump($_POST);
                            ?>
                        </code>
                    </pre>
                </div>
            </div>
            -->
        </div>
    </body>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js'></script>
</html>
