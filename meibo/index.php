<!DOCTYPE html>
<?php require_once('./lib/system.php') ?>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>社員名簿システム</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>

    <body>
        <?php
            // SQLクエリをぶん投げて結果の連想配列を受け取る
            function query($pdo, $query) {
                $sql = $pdo -> prepare($query);
                $sql -> execute();
                $result = $sql -> fetchAll();

                return $result;
            }
        ?>

        <?php
            $query = 'SELECT M.member_id, M.name, GM.grade_name, SM.section_name '
                    .'FROM member as M '
                    .'LEFT JOIN grade_master as GM ON M.grade_id = GM.id '
                    .'LEFT JOIN section1_master as SM ON M.section_id = SM.id '
                    .'WHERE 1 = 1';

            // 氏名
            if(isset($_GET['name']) and $_GET['name'] != "") {
                $query .= " AND M.name LIKE '%" . $_GET['name'] . "%'";
            }

            // 性別
            if(isset($_GET['gender']) and $_GET['gender'] != "") {
                $query .= " AND M.gender = " . $_GET['gender'];
            }

            // 部署
            if(isset($_GET['section']) and $_GET['section'] != "") {
                $query .= " AND M.section_id = " . $_GET['section'];
            }

            // 役職
            if(isset($_GET['grade']) and $_GET['grade'] != "") {
                $query .= " AND M.grade_id = " . $_GET['grade'];
            }
        ?>

        <!-- 共通ナビゲーションバー -->
        <?php include('./navbar.php'); ?>

        <div class="container my-3">

            <?php if(DEBUG): ?>
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                    <p><strong>SQL QUERY</strong></p>
                    <hr>
                    <pre style="white-space: pre-wrap;"><?php echo $query; ?></pre>
                </div>
            <?php endif; ?>

            <div class="card my-3 mx-auto" style="width: 30rem;">
                <h3 class="card-header bg-primary text-white">社員検索</h3>

                <div class="card-body">
                    <form action="#" method="get" id="form-search">
                        <!-- 名前 -->
                        <div class="form-group">
                            <label for="search-name" class="">名前</label>
                            <input name="name" class="form-control form-control-sm" type="text" id="search-name" placeholder="名前" value=<?php echo (isset($_GET['name']) and $_GET['name'] != "") ? $_GET['name'] : "" ?>>
                        </div>

                        <div class="form-row">
                            <!-- 性別 -->
                            <div class="form-group col-4">
                                <label for="search-gender" class="col-form-label">性別</label>

                                <select class="form-control form-control-sm" name="gender" id="search-gender" class="custom-select">
                                    <?php if(isset($_GET['gender']) and $_GET['gender'] != ""): ?>
                                        <option value="">すべて</option>>
                                    <?php else: ?>
                                        <option value="" selected>すべて</option>>
                                    <?php endif; ?>

                                    <?php $i = 0;?>

                                    <?php foreach(ARRAY_GENDER as $gender): ?>
                                        <?php $i++; ?>
                                        <?php if(isset($_GET['gender']) and $_GET['gender'] != ""): ?>
                                            <option value=<?php echo $i; ?><?php echo ($i == $_GET['gender']) ? " selected" : "" ?>><?php echo $gender; ?></option>
                                        <?php else: ?>
                                            <option value=<?php echo $i; ?>><?php echo $gender; ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <!-- 部署 -->
                            <div class="form-group col-4">
                                <label for="search-section" class="col-form-label">部署</label>

                                <select class="form-control form-control-sm" name="section" id="search-section" class="custom-select">
                                    <?php if(isset($_GET['section']) and $_GET['section'] != ""): ?>
                                        <option value="">すべて</option>>
                                    <?php else: ?>
                                        <option value="" selected>すべて</option>>
                                    <?php endif; ?>

                                    <?php $i = 0;?>

                                    <?php foreach(query($pdo, 'SELECT section_name FROM section1_master') as $section): ?>
                                        <?php $i++; ?>
                                        <?php if(isset($_GET['section']) and $_GET['section'] != ""): ?>
                                            <option value=<?php echo $i; ?><?php echo ($i == $_GET['section']) ? " selected" : "" ?>><?php echo $section[0]; ?></option>
                                        <?php else: ?>
                                            <option value=<?php echo $i; ?>><?php echo $section[0]; ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <!-- 役職 -->
                            <div class="form-group col-4">
                                <label for="search-grade" class="col-form-label">役職</label>

                                <select class="form-control form-control-sm" name="grade" id="search-grade" class="custom-select">
                                    <?php if(isset($_GET['grade']) and $_GET['grade'] != ""): ?>
                                        <option value="">すべて</option>>
                                    <?php else: ?>
                                        <option value="" selected>すべて</option>
                                    <?php endif; ?>

                                    <?php $i = 0;?>

                                    <?php foreach(query($pdo, 'SELECT grade_name FROM grade_master') as $grade): ?>
                                        <?php $i++; ?>
                                        <?php if(isset($_GET['grade']) and $_GET['grade'] != ""): ?>
                                            <option value=<?php echo $i; ?><?php echo ($i == $_GET['grade']) ? " selected" : "" ?>><?php echo $grade[0]; ?></option>
                                        <?php else: ?>
                                            <option value=<?php echo $i; ?>><?php echo $grade[0]; ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>


                        <div class="float-right">
                            <button type="submit" class="btn btn-primary">検索</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="card my-3">
                <?php $member_list = query($pdo, $query) ?>

                <h3 class="card-header bg-secondary text-white">社員一覧</h3>

                <div class="card-body">
                    <?php if(empty($member_list)): ?>
                        <p>検索条件に該当する社員がいません。</p>

                    <?php else: ?>
                        <p><?php echo ("検索結果: " . count($member_list)); ?></p>
                        <table class="table table-striped table-sm">
                            <thead class="thead-dark">
                                <tr>
                                    <th>社員ID</th>
                                    <th>氏名</th>
                                    <th>部署</th>
                                    <th>役職</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($member_list as $item): ?>
                                    <tr>
                                        <td><?php echo $item['member_id'];?></td>
                                        <td><a href="./detail01.php?id=<?php echo $item['member_id']?>"><?php echo $item['name'];?></a></td>
                                        <td><?php echo $item['section_name'];?></td>
                                        <td><?php echo $item['grade_name'];?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div>
            </div>

            <?php if(DEBUG): ?>
                <div class="card my-3">
                    <h3 class="card-header bg-info text-white">var_dump : output</h3>

                    <div class="card-body">
                        <pre class="border border-info rounded">
                            <code>
                                <!-- <?php echo "- GET -"; ?> -->
                                <?php var_dump($_GET); ?>

                                <!-- <?php echo "- SQL RESULT -"; ?> -->
                                <!-- <?php var_dump($menu); ?> -->
                            </code>
                        </pre>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </body>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js'></script>
</html>
