<!DOCTYPE html>

<?php require_once('./lib/system.php') ?>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>社員名簿システム</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>

    <body>

        <?php
            // 'id' が空ではないか確認
            $isValid = (isset($_GET['id']) and $_GET['id']);

            if($isValid) {
                $query = 'SELECT M.member_id, M.name, M.pref, M.gender, M.age, GM.grade_name, SM.section_name '
                        .'FROM member as M '
                        .'LEFT JOIN grade_master as GM ON M.grade_id = GM.id '
                        .'LEFT JOIN section1_master as SM ON M.section_id = SM.id '
                        .'WHERE 1 = 1 AND M.member_id = :id';

                $sql = $pdo -> prepare($query);
                $sql -> bindParam(':id', $_GET['id']);
                $sql -> execute();
                $result = $sql -> fetchAll();
            }
        ?>

        <!-- 共通ナビゲーションバー -->
        <?php include('./navbar.php'); ?>

        <div class="container my-3">
            <?php if(DEBUG and $isValid): ?>
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                    <p><strong>SQL QUERY</strong></p>
                    <hr>
                    <pre style="white-space: pre-wrap;"><?php echo $query; ?></pre>
                </div>
            <?php endif; ?>

            <div class="card my-3">
                <h3 class="card-header bg-primary text-white">社員情報</h3>

                <div class="card-body">
                    <?php if(empty($result)): ?>
                        <p>該当する社員情報が存在しません。</p>
                    <?php else: ?>
                        <table class="table table-sm">
                            <tr>
                                <th>社員ID</th>
                                <td><?php echo($result[0]['member_id']) ?></td>
                            </tr>
                            <tr>
                                <th>名前</th>
                                <td><?php echo($result[0]['name']) ?></td>
                            </tr>
                            <tr>
                                <th>出身地</th>
                                <td><?php echo(ARRAY_PREFS[$result[0]['pref']]) ?></td>
                            </tr>
                            <tr>
                                <th>性別</th>

                                <?php if($result[0]['gender'] == 1): ?>
                                    <td>男</td>
                                <?php elseif($result[0]['gender'] == 2): ?>
                                    <td>女</td>
                                <?php endif; ?>
                            </tr>
                            <tr>
                                <th>年齢</th>
                                <td><?php echo($result[0]['age']) ?></td>
                            </tr>
                            <tr>
                                <th>所属部署</th>
                                <td><?php echo($result[0]['section_name']) ?></td>
                            </tr>
                            <tr>
                                <th>役職</th>
                                <td><?php echo($result[0]['grade_name']) ?></td>
                            </tr>
                        </table>

                        <!-- 編集 -->
                        <div class="float-right">

                            <a href="./entry_update01.php?id=<?php echo $_GET['id'] ?>">
                                <button type="button" class="btn page-link text-dark d-inline-block">編集</button>
                            </a>

                            <button type="button" class="btn btn-danger text-white d-inline-block">削除</button>
                        </div>
                    <?php endif; ?>

                </div>
            </div>

            <?php if(DEBUG): ?>
                <div class="card my-3">
                    <h3 class="card-header bg-info text-white">var_dump : output</h3>

                    <div class="card-body">
                        <pre class="border border-info rounded">
                            <code>
                                <!-- <?php echo "- GET -"; ?> -->
                                <?php var_dump($_GET); ?>

                                <?php if($isValid): ?>
                                    <!-- <?php echo "- SQL RESULT -"; ?> -->
                                    <?php var_dump($result); ?>
                                <?php endif; ?>
                            </code>
                        </pre>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </body>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js'></script>
</html>
