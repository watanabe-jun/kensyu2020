<!DOCTYPE html>

<?php require_once('./lib/system.php') ?>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>社員名簿システム</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>

    <body>
        <!-- 共通ナビゲーションバー -->
        <?php include('./navbar.php'); ?>

        <div class="container my-3">
            <?php
                // 入力データのチェック と 登録
                if(!empty($_POST) and ($_POST['name'] != "" and $_POST['age'] != "" and $_POST['pref'] != "")) {
                    $query = 'INSERT INTO member (member_id, name, pref, gender, age, section_id, grade_id) '
                            .'VALUES (null, :name, :pref, :gender, :age, :section_id, :grade_id)';

                    $sql = $pdo -> prepare($query);
                    $sql -> bindValue(':name', $_POST['entry-name'], PDO::PARAM_STR);
                    $sql -> bindValue(':pref', $_POST['entry-pref'], PDO::PARAM_INT);
                    $sql -> bindValue(':gender', $_POST['entry-gender'], PDO::PARAM_INT);
                    $sql -> bindValue(':age', $_POST['entry-age'], PDO::PARAM_INT);
                    $sql -> bindValue(':section_id', $_POST['entry-section'], PDO::PARAM_INT);
                    $sql -> bindValue(':grade_id', $_POST['entry-grade'], PDO::PARAM_INT);
                    $sql -> execute();

                    $id = $pdo -> lastInsertId('member_id');
                    $url = "./detail01.php?id=" . $id;
                    header('Location:' . $url);
                    exit;
                }
            ?>

            <?php if(TRUE): ?>
                <div class="card my-3">
                    <h3 class="card-header bg-info text-white">var_dump : output</h3>

                    <div class="card-body">
                        <pre class="border border-info rounded">
                            <code>
                                <!-- <?php echo "- POST -"; ?> -->
                                <?php var_dump($_POST); ?>
                            </code>
                        </pre>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </body>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js'></script>
</html>
