<!DOCTYPE html>

<?php require_once('./lib/system.php') ?>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>社員名簿システム</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>

    <body>

        <?php
            // 'id' が空ではないか確認
            $isValid = (isset($_GET['id']) and $_GET['id']);

            if($isValid) {
                $query = 'SELECT M.member_id, M.name, M.pref, M.gender, M.age, GM.grade_name, M.grade_id, SM.section_name, M.section_id '
                        .'FROM member as M '
                        .'LEFT JOIN grade_master as GM ON M.grade_id = GM.id '
                        .'LEFT JOIN section1_master as SM ON M.section_id = SM.id '
                        .'WHERE 1 = 1 AND M.member_id = :id';

                $sql = $pdo -> prepare($query);
                $sql -> bindParam(':id', $_GET['id']);
                $sql -> execute();
                $result = $sql -> fetchAll();
            }
        ?>

        <!-- 共通ナビゲーションバー -->
        <?php include('./navbar.php'); ?>

        <div class="container my-3">
            <?php if(DEBUG and $isValid): ?>
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                    <p><strong>SQL QUERY</strong></p>
                    <hr>
                    <pre style="white-space: pre-wrap;"><?php echo $query; ?></pre>
                </div>
            <?php endif; ?>

            <div class="card my-3 mx-auto" style="width: 30rem;">
                <h3 class="card-header bg-primary text-white">社員情報</h3>

                <div class="card-body">
                    <?php if(empty($result)): ?>
                        <p>
                            <?php if(!isset($_GET['id']) or $_GET['id'] == ""): ?>
                                社員IDが指定されていません。
                            <?php elseif($_GET['id'] < 1): ?>
                                社員IDは整数で「1」以上を指定してください。
                            <?php else: ?>
                                該当する社員が存在しません。
                            <?php endif; ?>
                        </p>
                    <?php else: ?>
                        <form class="" action="#" method="post" id="form-entry">
                            <!-- 名前 -->
                            <div class="form-group row">
                                <label for="entry-name" class="col-sm-3 col-form-label">名前</label>

                                <div class="col-md-9">
                                    <!-- <?php echo (isset($_GET['name']) and $_GET['name'] != "") ? $_GET['name'] : ""; ?> -->
                                    <input name="name" class="form-control form-control-sm" type="text" id="entry-name" placeholder="名前" value="<?php echo $result[0]['name']; ?>">
                                </div>
                            </div>

                            <!-- 出身地 -->
                            <div class="form-group row">
                                <label for="entry-pref" class="col-sm-3 col-form-label">出身地</label>

                                <div class="col-md-9">
                                    <select class="form-control form-control-sm" name="pref" id="entry-pref" class="custom-select">
                                        <option value="">都道府県</option>

                                        <?php $i = 0;?>
                                        <?php foreach(ARRAY_PREFS as $pref): ?>
                                            <?php $i++; ?>
                                            <option value=<?php echo $i; ?> <?php echo($result[0]['pref']) == $i ? "selected" : "" ?>><?php echo $pref; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <!-- 性別 -->
                            <div class="form-group row">
                                <label for="entry-gender" class="col-sm-3 col-form-label">性別</label>

                                <div class="col-md-9">
                                    <?php $i = 0;?>
                                    <?php foreach(ARRAY_GENDER as $gender): ?>
                                        <?php $i++; ?>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="entry-gender" id="entry-gender-0<?php echo $i; ?>" value="<?php echo $i; ?>" <?php echo($result[0]['gender'] == $i ? "checked" : ""); ?>>
                                            <label class="form-check-label"><?php echo $gender; ?></label>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>

                            <!-- 年齢 -->
                            <div class="form-group row">
                                <label for="entry-age" class="col-sm-3 col-form-label">年齢</label>

                                <div class="col-md-9">
                                    <input name="name" class="form-control form-control-sm" type="number" id="entry-age" placeholder="年齢" value="<?php echo $result[0]['age']; ?>" onkeydown="return event.keyCode !== 69">
                                </div>
                            </div>

                            <!-- 所属部署 -->
                            <?php
                                $query_section = 'SELECT * '
                                                .'FROM section1_master';

                                $sql_section = $pdo -> prepare($query_section);
                                $sql_section -> execute();
                                $sections = $sql_section -> fetchAll();
                            ?>
                            <div class="form-group row">
                                <label for="entry-section" class="col-sm-3 col-form-label">所属部署</label>

                                <div class="col-md-9">
                                    <?php foreach($sections as $section): ?>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="entry-section" id="entry-section-0<?php echo($section['id']); ?>" value="<?php echo($section['id']); ?>" <?php echo($section['id'] == $result[0]['section_id'] ? "checked" : "") ?>>
                                            <label class="form-check-label"><?php echo($section['section_name']); ?></label>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>

                            <!-- 役職 -->
                            <?php
                                $query_grade = 'SELECT * '
                                                .'FROM grade_master';

                                $sql_grade = $pdo -> prepare($query_grade);
                                $sql_grade -> execute();
                                $grades = $sql_grade -> fetchAll();
                            ?>
                            <div class="form-group row">
                                <label for="entry-grade" class="col-sm-3 col-form-label">所属部署</label>

                                <div class="col-md-9">
                                    <?php foreach($grades as $grade): ?>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="entry-grade" id="entry-grade-0<?php echo($grade['id']); ?>" value="<?php echo($grade['id']); ?>" <?php echo($grade['id'] == $result[0]['grade_id'] ? "checked" : "") ?>>
                                            <label class="form-check-label"><?php echo($grade['grade_name']); ?></label>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>

                            <div class="float-right">
                                <button type="button" class="btn btn-primary text-white d-inline-block">登録</button>
                            </div>
                        </form>
                    <?php endif; ?>
                </div>
            </div>

            <?php if(DEBUG): ?>
                <div class="card my-3">
                    <h3 class="card-header bg-info text-white">var_dump : output</h3>

                    <div class="card-body">
                        <pre class="border border-info rounded">
                            <code>
                                <!-- <?php echo "- GET -"; ?> -->
                                <?php var_dump($_GET); ?>

                                <?php if($isValid): ?>
                                    <!-- <?php echo "- SQL RESULT -"; ?> -->
                                    <?php var_dump($result); ?>
                                <?php endif; ?>
                                <?php var_dump($sections); ?>
                            </code>
                        </pre>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </body>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js'></script>
</html>
