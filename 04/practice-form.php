<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>社内開発研修 04</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>

    <?php
        define('DB_HOST', 'localhost');
        define('DB_NAME', 'test');
        define('DB_USER', 'php_user');
        define('DB_PASS', 'wJYICpY2huzxqqxc');

        define('DB_DSN', 'mysql:host=localhost; dbname=test; charset=utf8');
    ?>

    <body>
        <?php
            // データベースへ接続
            try {
                $pdo = new PDO(DB_DSN, DB_USER, DB_PASS);

                // 例外を投げるようにする
                $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                echo $e -> getMessage();
                exit;
            }
        ?>

        <?php
            // SQLクエリをぶん投げて結果の連想配列を受け取る
            function query($pdo, $query) {
                $sql = $pdo -> prepare($query);
                $sql -> execute();
                $result = $sql -> fetchAll();

                return $result;
            }
        ?>

        <?php
            $query = 'SELECT * FROM test_table WHERE 1 = 1';

            // 料理名
            if(isset($_GET['name']) and $_GET['name'] != "") {
                $query .= " AND dish_name LIKE '%" . $_GET['name'] . "%'";
            }

            // ジャンル
            if(isset($_GET['genre']) and $_GET['genre'] != "") {
                $query .= " AND genre = '" . $_GET['genre'] . "'";
            }

            // 値段の上限下限
            if((isset($_GET['price-min']) and $_GET['price-min'] != "") and (isset($_GET['price-max']) and $_GET['price-max'] != "")) {
                $query .= " AND " . $_GET['price-min'] . " <= price AND price <= " . $_GET['price-max'];
            } else if (isset($_GET['price-min']) and $_GET['price-min'] != "") {
                $query .= " AND " . $_GET['price-min'] . " <= price";
            } else if (isset($_GET['price-max']) and $_GET['price-max'] != "") {
                $query .= " AND price <= " . $_GET['price-max'];
            }
        ?>

        <div class="container my-3">
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                <p><strong>SQL QUERY</strong></p>
                <hr>
                <pre style="white-space: pre-wrap;"><?php echo $query; ?></pre>
            </div>

            <div class="card my-3">
                <h3 class="card-header bg-primary text-white">メニュー検索</h3>

                <div class="card-body">
                    <form action="#" method="get" id="form-search">
                        <!-- 料理名 -->
                        <div class="form-group">
                            <label for="search-name">料理名</label>
                            <input name="name" class="form-control" type="text" id="search-name" placeholder="料理名" value=<?php echo (isset($_GET['name']) and $_GET['name'] != "") ? $_GET['name'] : "" ?>>
                        </div>

                        <!-- ジャンル -->
                        <div class="form-group">
                            <label for="search-genre">ジャンル</label>

                            <select name="genre" id="search-genre" class="custom-select">

                                <?php if(isset($_GET['genre']) and $_GET['genre'] != ""): ?>
                                    <option value="">ジャンルを選んでください。</option>>
                                <?php else: ?>
                                    <option value="" selected>ジャンルを選んでください。</option>>
                                <?php endif; ?>

                                <?php foreach(query($pdo, 'SELECT DISTINCT genre FROM test_table') as $genre): ?>
                                    <?php if(isset($_GET['genre']) and $_GET['genre'] != ""): ?>
                                        <option value=<?php echo '"'. $genre[0] . '"' ?><?php echo ($genre[0] == $_GET['genre']) ? " selected" : "" ?>><?php echo $genre[0] ?></option>
                                    <?php else: ?>
                                        <option value=<?php echo '"'. $genre[0] . '"' ?>><?php echo $genre[0] ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <!-- 値段 -->
                        <div class="form-group">
                            <label for="search-price">価格</label>

                            <div id="search-price" class="input-group mb-3">
                                <input name="price-min" type="number" class="form-control" aria-label="下限" value=<?php echo (isset($_GET['price-min']) and $_GET['price-min'] != "") ? $_GET['price-min'] : "" ?>>

                                <div class="input-group-prepend">
                                    <span class="input-group-text">～</span>
                                </div>

                                <input name="price-max" type="number" class="form-control" aria-label="上限" value=<?php echo (isset($_GET['price-max']) and $_GET['price-max'] != "") ? $_GET['price-max'] : "" ?>>
                            </div>
                        </div>

                        <div class="float-right">
                            <button type="submit" class="btn btn-primary">検索</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="card my-3">
                <?php $menu = query($pdo, $query) ?>

                <h3 class="card-header bg-secondary text-white">居酒屋ウェブレッジ 水道橋店 : メニュー</h3>

                <div class="card-body">
                    <?php if(empty($menu)): ?>
                        <p>検索条件に該当するメニューがありません。</p>
                    <?php else: ?>
                        <table class="table table-striped table-sm">
                            <thead class="thead-dark">
                                <tr>
                                    <th>料理名</th>
                                    <th>値段</th>
                                    <th>ジャンル</th>
                                    <th>メモ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($menu as $item): ?>
                                    <tr>
                                        <td><?php echo $item['dish_name'];?></td>
                                        <td><?php echo $item['price'] . ' 円';?></td>
                                        <td><?php echo $item['genre'];?></td>
                                        <td><?php echo $item['memo'];?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div>
            </div>

            <div class="card my-3">
                <h3 class="card-header bg-info text-white">var_dump : output</h3>

                <div class="card-body">
                    <pre class="border border-info rounded">
                        <code>
                            <!-- <?php echo "- GET -"; ?> -->
                            <?php var_dump($_GET); ?>

                            <!-- <?php echo "- SQL RESULT -"; ?> -->
                            <!-- <?php var_dump($menu); ?> -->
                        </code>
                    </pre>
                </div>
            </div>


        </div>
    </body>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js'></script>
</html>
