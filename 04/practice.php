<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>社内開発研修 04</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">    </head>
    </head>

    <?php
        define('DB_HOST', 'localhost');
        define('DB_NAME', 'test');
        define('DB_USER', 'php_user');
        define('DB_PASS', 'wJYICpY2huzxqqxc');

        define('DB_DSN', 'mysql:host=localhost; dbname=test; charset=utf8');
    ?>

    <body>
        <?php
            // データベースへ接続
            try {
                $pdo = new PDO(DB_DSN, DB_USER, DB_PASS);

                // 例外を投げるようにする
                $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                echo $e -> getMessage();
                exit;
            }
        ?>

        <?php
            // SQLクエリ
            $query = 'SELECT * FROM test_table';
        ?>

        <div class="container my-3">
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                <p><strong>SQL QUERY</strong></p>
                <hr>
                <pre><?php echo $query; ?></pre>
            </div>

            <?php
                $sql = $pdo -> prepare($query);
                $sql -> execute();
                $result = $sql -> fetchAll();
            ?>

            <div class="card my-3">
                <h3 class="card-header bg-primary text-white">居酒屋ウェブレッジ 水道橋店 : メニュー</h3>

                <div class="card-body">
                    <div class="row">
                        <?php foreach($result as $item): ?>
                            <div class="col-sm-4">
                                <p><?php echo $item['dish_name']; ?></p>
                            </div>

                            <div class="col-sm-2">
                                <p><?php echo $item['price'] . "円"; ?></p>
                            </div>

                            <hr>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>

            <div class="card my-3">
                <h3 class="card-header bg-info text-white">var_dump : output</h3>

                <div class="card-body">
                    <pre class="border border-info rounded">
                        <code>
                            <?php
                                echo var_dump($result);
                            ?>
                        </code>
                    </pre>
                </div>
            </div>

        </div>
    </body>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js'></script>
</html>
