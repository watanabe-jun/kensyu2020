<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>消費税計算</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">    </head>

    <body>
        <div class="container">
            <div class="card my-3">
                <h3 class="card-header bg-primary text-white">消費税計算</h3>

                <?php
                    # 最大値 (共通、テーブルの数やループ処理回数など)
                    $max = 5;
                ?>

                <div class="card-body">

                    <div class="alert alert-warning" role="note">
                        <p class="mb-0">※ ラジオボタンが未入力の場合、10%として計算します。</p>
                    </div>

                    <form action="#" method="post" id="tax_form">
                        <table class="table table-sm table-bordered table-hover" style="table-layout:fixed;">
                            <tr class="thead-dark text-center">
                                <th>商品</th>
                                <th>価格 (単位:円、税抜)</th>
                                <th>個数</th>
                                <th>税率</th>
                            </tr>

                            <?php for($i = 1; $i <= $max; $i++): ?>

                                <?php
                                    # 0埋めされた番号
                                    $number = str_pad($i, 2, 0, STR_PAD_LEFT);
                                    # product[00-99]
                                    $name = 'product' . $number;
                                    $keys = array("name", "value", "quantity", "tax_rate");

                                    # 初期値
                                    $values = array(
                                        "name" => "",
                                        "value" => 0,
                                        "quantity" => 0,
                                        "tax_rate" => 0.10
                                    );

                                    if (!empty($_POST[$name])) {
                                        foreach($keys as $key) {
                                            if(!empty($_POST[$name][$key])) {
                                                $values[$key] = $_POST[$name][$key];
                                            }
                                        }
                                    }
                                ?>

                                <tr class="text-center">
                                    <td><input name="<?php echo $name ?>[name]" type="text" value=<?php echo $values['name'] ?>></td>
                                    <td><input name="<?php echo $name ?>[value]" type="number" value=<?php echo $values['value'] ?> style="text-align: right; " required min="0"></td>
                                    <td>
                                        <input name="<?php echo $name ?>[quantity]" type="number" value=<?php echo $values['quantity'] ?>  style="text-align: right; " required min="0">
                                        個
                                    </td>

                                    <td>
                                        <input id=<?php echo "tax" . $number ."-1" ?> name="<?php echo $name ?>[tax_rate]" type="radio" value=0.08 <?php echo (($values['tax_rate'] === "0.08") ? "checked" : "") ?>><label for=<?php echo "tax" . $number ."-1" ?> data-default=>8%</label>
                                        <input id=<?php echo "tax" . $number ."-2" ?> name="<?php echo $name ?>[tax_rate]" type="radio" value=0.10 <?php echo (($values['tax_rate'] === "0.10") ? "checked" : "") ?> data-default><label for=<label for=<?php echo "tax" . $number ."-2" ?>>10%</label>
                                    </td>
                                </tr>
                            <?php endfor; ?>
                        </table>

                    <div class="float-left">
                        <button type="submit" class="btn btn-primary">送信</button>
                        <button type="button" class="btn page-link text-dark d-inline-block clear-button" value="clear">リセット</button>
                    </div>

                    </form>
                </div>
            </div>

            <hr>

            <div class="card my-3">
                <h3 class="card-header bg-secondary text-white">消費税計算</h3>
                <div class="card-body">
                    <table class="table table-bordered table-sm" style="table-layout:fixed;">
                        <tr class="thead-dark text-center">
                            <th>商品</th>
                            <th>価格 (単位:円、税抜)</th>
                            <th>個数</th>
                            <th>税率</th>
                            <th>小計 (単位: 円)</th>
                        </tr>

                        <?php
                            for ($i = 1; $i <= $max; $i++) {
                                $key = 'product' . str_pad($i, 2, 0, STR_PAD_LEFT);

                                if(!empty($_POST[$key])) {
                                    if(empty($_POST[$key]['tax_rate'])) {
                                        $_POST[$key]['tax_rate'] = "0.10";
                                    }
                                    $_POST[$key]['subtotal'] = (intval($_POST[$key]['value']) * intval($_POST[$key]['quantity'])) * (intval($_POST[$key]['tax_rate']) + 1);
                                }
                            }
                        ?>

                        <?php for($i = 1; $i <= $max; $i++): ?>
                            <?php $product = 'product' . str_pad($i, 2, 0, STR_PAD_LEFT); ?>
                            <tr>
                                <?php if (!empty($_POST[$product])): ?>
                                    <?php foreach ($_POST[$product] as $key => $value): ?>
                                        <?php if ($key === 'tax_rate'): ?>
                                            <td><div class="text-right"><?php echo $value . '%'; ?></div></td>
                                        <?php elseif (($key === 'value') or ($key === 'subtotal')) : ?>
                                            <td><div class="text-right"><?php echo number_format($value) . "円"; ?></div></td>
                                        <?php elseif ($key === 'quantity'): ?>
                                            <td><div class="text-right"><?php echo $value; ?></div></td>
                                        <?php else: ?>
                                            <td><?php echo $value; ?></td>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tr>
                        <?php endfor; ?>

                        <?php
                            $total = 0;

                            for ($i = 1; $i <= $max; $i++) {
                                $key = 'product' . str_pad($i, 2, 0, STR_PAD_LEFT);

                                if(!empty($_POST[$key])) {
                                    $total += $_POST[$key]['subtotal'];
                                }
                            }
                        ?>

                        <tr>
                            <td colspan="4"><strong>合計</strong></td>
                            <td><div class="text-right"><strong><?php echo number_format($total) . "円"; ?></strong></div></td>
                        </tr>

                    </table>
                </div>
            </div>

            <div class="card my-3">
                <h3 class="card-header bg-info text-white">var_dump : output</h3>

                <div class="card-body">
                    <pre class="border border-info rounded">
                        <code>
                            <?php
                                echo var_dump($_POST);
                            ?>
                        </code>
                    </pre>
                </div>
            </div>

        </div>
    </body>

    <script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js'></script>

    <!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    <script type="text/javascript">
        "use strict";

        $(function () {
            $(".clear-button").on("click", function () {
                clearForm(this.form);
            });

            function clearForm (form) {
                $(form)
                    .find("input, select, textarea")
                    .not(":button, :submit, :reset, :hidden")
                    .val("")
                    .prop("checked", false)
                    .prop("selected", false)
                ;

                // $(form).find(":radio").filter("[data-default]").prop("checked", true);
                $(form).find("input").filter('[type="number"]').val("0");
            }
        });
    </script>
</html>
